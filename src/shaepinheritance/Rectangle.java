/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shaepinheritance;

/**
 *
 * @author tud08
 */
public class Rectangle extends Shape{
    protected double width;
    protected double height;
    
    public Rectangle(double width, double height){
        if (width==0){
            System.out.println("Width is zero!!");
        }if (height==0){
            System.out.println("Height is zero!!");
        }
        this.width = width;
        this.height = height;
    }
    public double calArea(){
        return this.width*this.height;
    }
    public void tostring(){
        System.out.println("Rectangle width: "+this.width+" height: "+this.height+" Area is: "+this.calArea());
    }
}
