/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shaepinheritance;

/**
 *
 * @author tud08
 */
public class Triangle extends Shape{
    private double base;
    private double height;
    
    public Triangle(double base, double height){
        if (base==0||height==0){
            System.out.println("Error size must more than zero!!");
            return;
        }
        this.base = base;
        this.height = height;
    }
    public double calArea(){
        return base*height/2;
    }
    public void tostring(){
        System.out.println("Trianle base: "+this.base+" height: "+this.height+" Area is: "+this.calArea());
    }
}
